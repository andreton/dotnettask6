﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask4YellowPagesUpdated
{
    class Person //Object for storing person, made all public just for this exercise, better practice to use protected.
    {
        public string firstName { get ; set; }

        public string lastName { get; set; }

        public string telephone { get; set; }

        public string address { get; set; }

        public Person(string firstName, string lastName) //Default constructor
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Person(string firstName, string lastName, string telephone, string address)//Overload constructor with additional information 
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.telephone = telephone;
            this.address = address;
        }

        public override string ToString() //Making a tostring method based on which information is uploaded, first and last name are required, telephone and address are optional
        {
            if (telephone == null && address==null)
            {
                return "First name: " + firstName + " Last name: " + lastName+"\n";
            }
            else if(telephone==null)
            {
                return "First name: " + firstName + " Last name: " + lastName+ " Telephone: "+telephone + "\n";
            }
            else if (address == null)
            {
                return  "First name: " + firstName + " Last name: " + lastName + " Address: " + address + "\n";
            }
            else
            {
                return "First name: " + firstName + " Last name: " + lastName + " Address: " + address+" Telephone: "+ telephone + "\n";
            }

        }

    }
}
